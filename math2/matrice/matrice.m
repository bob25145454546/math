a =  [1,0,2;
        -1,1,3;
        0,-1,-1]
    
%a = [1,2    ;    -3,1]; 
    
disp(' ');
determinant = det(a);
inverse = inv(a)*determinant;

if abs(determinant) <= 0.000000000001
    determinant = 0;
end
    
comTrans = transpose(inverse);

msg = sprintf('\n determinant : %d', determinant);
disp(msg);

%msg = sprintf('\n matrice de cofacteur: \n');
%disp(msg);
%disp(int8(comTrans));

msg = sprintf('\n matrice inverse : \n \n 1/%d   x \n', determinant);
disp(msg);
disp(int8(inverse));

disp('---------------------------------------');

b= [1/sqrt(6),-5/sqrt(30),0;     
     -2/sqrt(6) ,-2/sqrt(30),1/sqrt(5);     
    1/sqrt(6),1/sqrt(30),2/sqrt(5)];

transposeeee = transpose(b);

a = [7,-2,1;
      -2,10,-2;
       1,-2,7];

disp(transposeeee*a*b);


%inverse = inv(transposeeee);

%disp(inverse*transposeeee*a*b)
%disp(a*b)